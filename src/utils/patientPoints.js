let patientPoints = [{
  id: '3f44b1fe9b3645f582e7a318b668c296',
  longitude: 120.783348,
  latitude: 27.946557,
  iconPath: '/static/icons/point.png',
  width: 14,
  height: 20,
  callout: {
    content: '今日问诊患者',
    bgColor: '#fff',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    display: 'ALWAYS',
    borderColor: '#fff',
    color: '#FC984C'
  }
}, {
  id: '3f44b1fe9b3645f582e7a318b668c296',
  longitude: 120.795536,
  latitude: 27.939961,
  iconPath: '/static/icons/point.png',
  width: 14,
  height: 20,
  callout: {
    content: '今日问诊患者',
    bgColor: '#fff',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    display: 'ALWAYS',
    borderColor: '#fff',
    color: '#FC984C'
  }
}, {
  id: '3f44b1fe9b3645f582e7a318b668c296',
  longitude: 120.778799,
  latitude: 27.944738,
  iconPath: '/static/icons/point.png',
  width: 14,
  height: 20,
  callout: {
    content: '今日问诊患者',
    bgColor: '#fff',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    display: 'ALWAYS',
    borderColor: '#fff',
    color: '#FC984C'
  }
}, {
  id: '3f44b1fe9b3645f582e7a318b668c296',
  longitude: 120.787725,
  latitude: 27.93033,
  iconPath: '/static/icons/point.png',
  width: 14,
  height: 20,
  callout: {
    content: '今日问诊患者',
    bgColor: '#fff',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    display: 'ALWAYS',
    borderColor: '#fff',
    color: '#FC984C'
  }
},  {
  id: '3f44b1fe9b3645f582e7a318b668c296',
  longitude:120.709443,
  latitude: 27.998123,
  iconPath: '/static/icons/point.png',
  width: 14,
  height: 20,
  callout: {
    content: '今日问诊患者',
    bgColor: '#fff',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    display: 'ALWAYS',
    borderColor: '#fff',
    color: '#FC984C'
  }
}]

export default patientPoints;
import request from '@/utils/request.js';

const login = data => request('/sys/login', {
  method: 'POST',
  body: data
});

export default {
  login
}
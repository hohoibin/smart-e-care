var CryptoJS = require("crypto-js");
const key = CryptoJS.enc.Latin1.parse('abcdef0123456789');
const iv = CryptoJS.enc.Latin1.parse('0123456789abcdef');

function decode(word) {
  // let encryptedHexStr = CryptoJS.enc.Hex.parse(word);
  // let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
  // let decrypted = CryptoJS.AES.decrypt(srcs, key, {
  //   iv: iv,
  //   mode: CryptoJS.mode.CBC,
  //   padding: CryptoJS.pad.ZeroPadding
  // });
  // let decryptedStr = decrypted.toString(CryptoJS.enc.Utf8);
  // console.log(decryptedStr);
  // return decryptedStr.toString();
  var decrypted = CryptoJS.AES.decrypt(word, key, {iv: iv, padding: CryptoJS.pad.ZeroPadding});
  return decrypted.toString(CryptoJS.enc.Utf8);
}

function encode(data) { //加密
  var parseKey = CryptoJS.enc.Utf8.parse(key);
  var parseIV = CryptoJS.enc.Utf8.parse(iv);
  var encrypted = CryptoJS.AES.encrypt(data, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.ZeroPadding
  });
  return encrypted.ciphertext.toString(); //返回的是hex格式的密文,encrypted.toString()返回base64格式
}

module.exports= {
  decode,
  encode
}
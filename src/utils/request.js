import { REQUEST_URL } from './config';
const fetch = function(url, options = {}) {
  const { method = "GET", body = {}, showError = true } = options;

  if(!url.startsWith('http')) {
    // url = 'http://192.168.0.224:8234/ws-lw-ifc' + url;
    // url = 'http://192.168.0.149:9527/ws-lw-ifc' + url;
    url = REQUEST_URL + url;
  }

  return new Promise((resolve, reject) => {
    let header = {
      "content-type": "application/x-www-form-urlencoded"
    }
    const token = wx.getStorageSync('token');
    if(token) {
      header.token = token
    }
    
    wx.request({
      url,
      data: body,
      method,
      header,
      success: res => {
        if (showError) {
          if (res.data.status < 200 || res.data.status > 300) {
            return reject(res.data.msg || {});
          }
        }
        return resolve(res.data);
 
      },
      complete: res => {
        // TODO:
      },
      fail: res => {
        reject(res);
        mpvue.showToast({
          title: res.errMsg,
          icon: 'none'
        });
      }
    });
  });
};

export default fetch;
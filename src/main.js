import Vue from 'vue'
import App from './App'
import Api from './api';

Vue.config.productionTip = false;
Vue.prototype.$api = Api;
Vue.prototype.$toast = {
  success: function(message) {
    if(message) {
      mpvue.showToast({
        title: message,
        icon: 'success'
      });
    }
  },
  error: function(message) {
    if(message) {
      mpvue.showToast({
        title: message,
        icon: 'none'
      });
    }
  }
};
App.mpType = 'app'

const app = new Vue(App)
app.$mount()

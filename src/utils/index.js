import groupTree from './groupTree';
function formatNumber(n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

export function formatTime(date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('/')
  const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1} ${t2}`
}

export function navigateToPatientDetail(patientId) {
  var pages = getCurrentPages(); //获取加载的页面

  var currentPage = pages[pages.length - 1]; //获取当前页面的对象

  var url = currentPage.route; //当前页面url
  if (url.indexOf("pages/my-patient/detail/main") > -1) {
    return;
  }
  mpvue.navigateTo({
    url: `/pages/my-patient/detail/main?patientId=${patientId}`
  });
}

export function getPageQuery(context) {
  const pages = getCurrentPages();
  const currentPage = pages[pages.length - 1];
  const options = currentPage.options
  return options
}

export function compareVersion(v1, v2) {
  v1 = v1.split('.')
  v2 = v2.split('.')
  const len = Math.max(v1.length, v2.length)

  while (v1.length < len) {
    v1.push('0')
  }
  while (v2.length < len) {
    v2.push('0')
  }

  for (let i = 0; i < len; i++) {
    const num1 = parseInt(v1[i])
    const num2 = parseInt(v2[i])

    if (num1 > num2) {
      return 1
    } else if (num1 < num2) {
      return -1
    }
  }

  return 0
}

export function formattedGenderDic(key) {
  switch(key) {
    case '10001':
      return '男';
    case '10002':
      return '女';
    default: 
      return '';
  }
}

export function formattedStatusDic(key) {
  switch(key) {
    case '13001':
      return '正常管理';
    case '13002':
      return '死亡';
    case '13007':
      return '走失';
    case '13003':
      return '连续三次未访到';
    case '13004':
      return '迁居他处';
    case '13005':
      return '外出打工';
    case '13006':
      return '其他';
    case '13008':
      return '回归管理';
    default: 
      return '';
  }
}

export function statusMap() {
  const statusMap = new Map();
  statusMap.set('13001', '正常管理');
  statusMap.set('13002', '死亡');
  statusMap.set('13003', '连续三次未访到');
  statusMap.set('13004', '迁居他处');
  statusMap.set('13005', '外出打工');
  statusMap.set('13006', '其他');
  statusMap.set('13007', '走失');
  statusMap.set('13008', '回归管理');
  return statusMap;
}

export function visitPersonTypeMap() {
  const visitPersonTypeMap = new Map();
  visitPersonTypeMap.set('17001', '患者本人');
  visitPersonTypeMap.set('17002', '患者家属或监护人');
  visitPersonTypeMap.set('17003', '其他知情人等');
  return visitPersonTypeMap;
}

export function lastVisitTypeMap() {
  const lastVisitTypeMap = new Map();
  lastVisitTypeMap.set('15001', '稳定');
  lastVisitTypeMap.set('15002', '基本稳定');
  lastVisitTypeMap.set('15003', '不稳定');
  return lastVisitTypeMap;
}

export function gradeMap() {
  const gradeMap = new Map();
  gradeMap.set('0' , '0级');
  gradeMap.set('1' , '1级');
  gradeMap.set('2' , '2级');
  gradeMap.set('3' , '3级');
  gradeMap.set('4' , '4级');
  gradeMap.set('5' , '5级');
  return gradeMap;
}


export function taskStatusMap() {
  const taskStatusMap = new Map();
  taskStatusMap.set('16002', '已完成');
  taskStatusMap.set('16001', '未完成');
  return taskStatusMap;
}

export function isAssetTypeAnImage(ext) {
  return [
  'png', 'jpg', 'jpeg', 'bmp', 'gif', 'webp', 'psd', 'svg', 'tiff'].
  indexOf(ext.toLowerCase()) !== -1;
}

export function isAssetTypeAvailable(ext) {
  return [
    'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf'].
    indexOf(ext.toLowerCase()) !== -1;
}

export function recursiveFind(expectId){
  let name = ''
  const getStr = function(list){
      list.forEach(function(row){
        if(row.children.length){
            getStr(row.children)
        }
        if(expectId == row.id) {
            name = row.text;
            return;
        }
      })
  }
  getStr(groupTree)
  return name;
}

/**
*
* @param fn {Function}   实际要执行的函数
* @param delay {Number}  延迟时间，也就是阈值，单位是毫秒（ms）
*
* @return {Function}     返回一个“去弹跳”了的函数
*/
export function debounce(fn, delay) {
  // 定时器，用来 setTimeout
  var timer
  // 返回一个函数，这个函数会在一个时间区间结束后的 delay 毫秒时执行 fn 函数
  return function () {
    // 保存函数调用时的上下文和参数，传递给 fn
    var context = this
    var args = arguments
    // 每次这个返回的函数被调用，就清除定时器，以保证不执行 fn
    clearTimeout(timer)
    // 当返回的函数被最后一次调用后（也就是用户停止了某个连续的操作），
    // 再过 delay 毫秒就执行 fn
    timer = setTimeout(function () {
      fn.apply(context, args)
    }, delay)
  }
}

export default {
  formatNumber,
  formatTime,
  navigateToPatientDetail,
  formattedGenderDic,
  formattedStatusDic,
  statusMap,
  lastVisitTypeMap,
  gradeMap,
  visitPersonTypeMap,
  taskStatusMap,
  isAssetTypeAnImage,
  isAssetTypeAvailable,
  debounce,
  compareVersion
}

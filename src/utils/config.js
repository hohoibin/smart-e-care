// 测试
export const REQUEST_URL = 'https://ws.rwlbs.com:8705/ws-ifc';
export const DOWNLOAD_FILE_URL = 'https://ws.rwlbs.com:8705/file';


// 生产
// export const REQUEST_URL = 'https://ws.weixin.rwlbs.com:8080/ws-ifc';
// export const DOWNLOAD_FILE_URL = 'https://ws.file.rwlbs.com:8080/file';


// DEV
// export const REQUEST_URL = 'http://192.168.0.224:8234/ws-lw-ifc';
// export const DOWNLOAD_FILE_URL = 'http://192.168.0.224:8234/file';

export default {
  REQUEST_URL,
  DOWNLOAD_FILE_URL
}
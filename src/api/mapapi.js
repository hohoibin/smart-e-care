import request from '@/utils/request';

const key = '475faa3e0d5120aebe3c260538b5fb4d';

const convertCooridate = data => request(`https://restapi.amap.com/v3/assistant/coordinate/convert?key=${key}&locations=${data.location}`, {
  method: 'GET',
  showError: false
});

const reGeoCode = data => request(`https://restapi.amap.com/v3/geocode/regeo?key=${key}&location=${data.location}`, {
  method: 'GET',
  showError: false
});


const getDistance = data => request(`https://restapi.amap.com/v3/distance?key=${key}&origins=${data.origins}&destination=${data.destination}&type=0`, {
  method: 'GET',
  showError: false
})


export default {
  reGeoCode,
  convertCooridate,
  getDistance
}
import request from '@/utils/request';

const getVisitList = data => request('/visit/list', {
  method: 'POST',
  body: data
});

const getVisitDetail = data => request('/visit/findById', {
  method: 'POST',
  body: data
});

const getDrugList = data => request('/sys/sysDrugs/list', {
  method: 'POST',
  body: data
});

const saveVisitRecord = data => request('/visit/save', {
  method: 'POST',
  body: data
})

const getVisitFileList = data => request('/visit/getFileList', {
  method: 'POST',
  body: data
});

const getVisitDrugList = data => request('/visitDrugs/list', {
  method: 'POST',
  body: data
})

const loadGroupTree = data => request('/sys/sysGroup/loadGroupTree', {
  method: 'POST',
  body: data
});

const findPersonsByGroupId = data => request('/sys/sysPerson/findSfsPersonByGroupId', {
  method: 'POST',
  body: data
});

export default {
  getVisitList,
  getVisitDetail,
  getDrugList,
  saveVisitRecord,
  getVisitFileList,
  getVisitDrugList,
  loadGroupTree,
  findPersonsByGroupId
}
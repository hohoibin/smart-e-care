import request from '@/utils/request.js';

const getTaskList = data => request('/task/list', {
  method: 'POST',
  body: data
});

const getTaskDetail = data => request('/task/taskDetail', {
  method: 'POST',
  body: data
});

const downloadAttachment = data => request('/sys/file/download', {
  method: 'GET',
  body: data
});

const getTaskCount = data => request('/task/getTaskCount', {
  method: 'POST',
  body: data
});

const updateFeedBack = data => request('/task/feedback', {
  method: 'POST',
  body: data
});

const readTask = data => request('/task/readTask', {
  method: 'POST',
  body: data
});

const saveTask = data => request('/task/save', {
  method: 'POST',
  body: data
})

export default {
  getTaskList,
  getTaskDetail,
  downloadAttachment,
  getTaskCount,
  updateFeedBack,
  saveTask,
  readTask
}
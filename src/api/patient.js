import request from '@/utils/request';

const getRecentVisitList = data => request('/culpritinfo/culprit/findVisitIn7Day', {
  method: 'POST',
  body: data
});

const getOvertimeVisitList = data => request('/culpritinfo/culprit/findVisitOut7Day', {
  method: 'POST',
  body: data
});


const getPatientDetail = data => request('/culpritinfo/culprit/findById', {
  method: 'POST',
  body: data
});

const saveOrUpdatePatientInfo = data => request('/culpritinfo/culprit/saveOrUpdate', {
  method: 'POST',
  body: data
})

const getPatientList = data => request('/culpritinfo/culprit/list', {
  method: 'POST',
  body: data
})

export default {
  getRecentVisitList,
  getOvertimeVisitList,
  getPatientDetail,
  saveOrUpdatePatientInfo,
  getPatientList
}
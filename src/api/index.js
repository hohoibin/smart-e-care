import login from './login';
import diagnosis from './diagnosis';
import task from './task';
import patient from './patient';
import my from './my';
import mapapi from './mapapi';

export default {
  ...login,
  ...diagnosis,
  ...task,
  ...patient,
  ...my,
  ...mapapi
}
import request from '@/utils/request';

const getMyCountById = data => request('/culpritinfo/culprit/selectMyCountByGroupIds', {
  method: 'POST',
  body: data
});

const getMyDetail = data => request('/sys/sysAccount/detail', {
  method: 'POST',
  body: data
});

const changePassword = data => request('/sys/sysAccount/updatePass', {
  method: 'POST',
  body: data
})

export default {
  getMyCountById,
  getMyDetail,
  changePassword
}